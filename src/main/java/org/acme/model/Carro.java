//package org.acme.model;
//
//import javax.persistence.Entity;
//import javax.persistence.ManyToOne;
//import javax.persistence.OneToOne;
//
//import io.quarkus.hibernate.orm.panache.PanacheEntity;
//import lombok.AllArgsConstructor;
//import lombok.Builder;
//import lombok.NoArgsConstructor;
//
//@NoArgsConstructor
//@AllArgsConstructor
//@Builder
//@Entity
//public class Carro extends PanacheEntity {
//	public String placa;
//	public int ano;
//	public String cor;
//	
//	@ManyToOne
//	public Modelo modelo;
//	
//	@OneToOne
//	public Pessoa pessoa;
//}
