//package org.acme.model;
//
//import javax.persistence.CascadeType;
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.OneToOne;
//
//import io.quarkus.hibernate.orm.panache.PanacheEntity;
//
//
//@Entity
//public class Apartamento extends PanacheEntity {
//      public int numero;
//      public String bloco;
//      public int andar;
//      
//      @OneToOne
//      public Condominio condominio;
//      @OneToOne(mappedBy = "apartamento", cascade = CascadeType.REFRESH, orphanRemoval = true, fetch = FetchType.LAZY)
//      public Pessoa pessoa;
//}
