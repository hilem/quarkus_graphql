//package org.acme.model;
//
//import java.util.List;
//
//import javax.persistence.CascadeType;
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.OneToMany;
//import javax.persistence.OneToOne;
//
//import io.quarkus.hibernate.orm.panache.PanacheEntity;
//import lombok.AllArgsConstructor;
//import lombok.Builder;
//import lombok.NoArgsConstructor;
//
//@AllArgsConstructor
//@NoArgsConstructor
//@Builder
//@Entity
//public class Municipio extends PanacheEntity {
//	
//	public String nome;
//	
//	@OneToOne
//	public Estado estado;
//	@OneToMany(mappedBy = "municipio", cascade = CascadeType.REFRESH, orphanRemoval = true, fetch = FetchType.EAGER)
//	public List<Endereco> enderecos;
//
//}
