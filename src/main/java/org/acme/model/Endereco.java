//package org.acme.model;
//
//import javax.persistence.Entity;
//import javax.persistence.OneToOne;
//
//import io.quarkus.hibernate.orm.panache.PanacheEntity;
//import lombok.AllArgsConstructor;
//import lombok.Builder;
//import lombok.NoArgsConstructor;
//
//@AllArgsConstructor
//@NoArgsConstructor
//@Builder
//@Entity
//public class Endereco extends PanacheEntity {
//	
//	public String endereco;
//	public String complemento;
//	public int numero;
//	public int cep;
//	
//	@OneToOne
//	public Municipio municipio;
//	
//	@OneToOne
//	public Condominio condominio;
//   
//}
