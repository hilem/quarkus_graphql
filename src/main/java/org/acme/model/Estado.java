//package org.acme.model;
//
//import java.util.List;
//
//import javax.persistence.CascadeType;
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.OneToMany;
//
//import io.quarkus.hibernate.orm.panache.PanacheEntity;
//import lombok.AllArgsConstructor;
//import lombok.Builder;
//import lombok.NoArgsConstructor;
//
//@AllArgsConstructor
//@NoArgsConstructor
//@Builder
//@Entity
//public class Estado extends PanacheEntity{
//	 
//	public String nome;
//	public String sigla;
//	@OneToMany(mappedBy = "estado", cascade = CascadeType.REFRESH, orphanRemoval = true, fetch = FetchType.EAGER)
//	public List<Municipio> municipio;
//
//}
