//package org.acme.model;
//
//import javax.persistence.CascadeType;
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.OneToOne;
//
//import io.quarkus.hibernate.orm.panache.PanacheEntity;
//import lombok.AllArgsConstructor;
//import lombok.Builder;
//import lombok.NoArgsConstructor;
//
//@AllArgsConstructor
//@NoArgsConstructor
//@Builder
//@Entity
//public class Pessoa extends PanacheEntity{
//	
//	public  String nome;
//	
//	public int idade;
//	
//	public String sexo;
//	
//	@OneToOne(mappedBy = "pessoa", cascade = CascadeType.REFRESH, orphanRemoval = true, fetch = FetchType.LAZY)
//	public Carro carro;
//	@OneToOne
//	public Apartamento apartamento;
//}
