package org.acme.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;


@Entity
@Table(name = "modelo")
public class Modelo extends PanacheEntityBase {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;
	public String nome;
	@ManyToOne
	public Marca marca;
}
