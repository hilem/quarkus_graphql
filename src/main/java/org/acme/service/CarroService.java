//package org.acme.service;
//
//import java.util.List;
//
//import javax.enterprise.context.ApplicationScoped;
//
//import org.acme.model.Carro;
//
//@ApplicationScoped
//public class CarroService {
//
//	public List<Carro> buscaTodasCarros() {
//		return Carro.listAll();
//	}
//
//	public Carro buscaCarro(Long CarroId) {
//
//		return Carro.findById(CarroId);
//	}
//
//	public List<Carro> buscaCarroNome(String carro) {
//
//		return Carro.find("nome LIKE ?1  ", "%"+carro+"%").list();
//	}
//
//	public void insereCarro(Carro carro) {
//		Carro.persist(carro);
//
//	}
//
//	public Carro atualizaCarro(Carro carro) {
//		Carro updateCarro = Carro.findById(carro.id);
//		if(!updateCarro.equals(null)) {
//			Carro.update("id = ?1", carro.id);		
//		}
//		return updateCarro;
//	}
//
//	public Carro apagaCarro(Long carroId) {
//		Carro buscaCarro = Carro.findById(carroId);
//		if(!buscaCarro.equals(null)){
//		  Carro.deleteById(carroId);
//		}
//		return buscaCarro;		
//	}
//}
