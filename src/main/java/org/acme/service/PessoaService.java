//package org.acme.service;
//import java.util.List;
//
//import javax.enterprise.context.ApplicationScoped;
//
//import org.acme.model.Pessoa;
//
//
//@ApplicationScoped
//public class PessoaService {
//		
//	
//	public Pessoa buscaPessoa(int pessoaId) {
//		return Pessoa.findById(pessoaId);
//	}
//	
//	public List<Pessoa> buscaTodasPessoa() {
//		return Pessoa.listAll();
//	}
//	
//	public void inserePessoa(Pessoa pessoa) {
//		Pessoa.persist(pessoa);
//	}
//	
//	public Pessoa atualizaPessoa(Pessoa pessoa) {
//		Pessoa buscaPessoa = Pessoa.findById(pessoa.id);
//		if(!buscaPessoa.equals(null)){
//			Pessoa.update("nome = ?1, idade = ?2, sexo = ?3 WHERE id = ?4 ", pessoa.nome, pessoa.idade, pessoa.sexo,pessoa.id);
//		}
//		return buscaPessoa;		
//	}
//	
//	public Pessoa apagaPessoa(int pessoaId) {
//		Pessoa buscaPessoa = Pessoa.findById(pessoaId);
//		if(!buscaPessoa.equals(null)){
//		  Pessoa.deleteById(buscaPessoa.id);
//		}
//		return buscaPessoa;		
//	}
//	
//
//}
