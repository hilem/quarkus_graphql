package org.acme.service;

import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.NotFoundException;

import org.acme.model.Modelo;

@ApplicationScoped
public class ModeloService {

	public List<Modelo> buscaTodosModelo() {
		return Modelo.listAll();
	}

	public Modelo buscaModelo(Long modeloId) {
		return Modelo.findById(modeloId);		
	}

	public List<Modelo> buscaModeloNome(String modelo) {
		return Modelo.find("nome LIKE ?1 ", "%"+modelo+"%").list();
	}

	public void insereModelo(Modelo modelo) {
		Modelo.persist(modelo);		
	}

	public void atualizaModelo(Long modeloId, Modelo modelo) {
		
		Optional<Modelo> buscaModelo = Modelo.findByIdOptional(modeloId);
		if(buscaModelo.isEmpty()) {
			throw new NotFoundException();		
		}
		Modelo updateModelo = buscaModelo.get();
		updateModelo.nome = modelo.nome;
		updateModelo.marca = modelo.marca;
		updateModelo.persist();
	}

	public void apagaModelo(Long modeloId) {
		Optional<Modelo> buscaModelo = Modelo.findByIdOptional(modeloId);
		buscaModelo.ifPresentOrElse(Modelo::delete, () ->{throw new NotFoundException();});	
	}

}
