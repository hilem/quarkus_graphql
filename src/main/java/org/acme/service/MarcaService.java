package org.acme.service;

import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.NotFoundException;

import org.acme.model.Marca;

@ApplicationScoped
public class MarcaService {

	public List<Marca> buscaTodasMarcas() {
		return Marca.listAll();
	}

	public Marca buscaMarca(Long marcaId) {

		return Marca.findById(marcaId);
	}

	public List<Marca> buscaMarcaNome(String marca) {

		return Marca.find("nome LIKE ?1  ", "%"+marca+"%").list();
	}

	public void insereMarca(Marca marca) {
		Marca.persist(marca);

	}

	public void atualizaMarca(Long marcaId, Marca marca) {
		Optional<Marca> buscaMarca = Marca.findByIdOptional(marcaId);
		if( buscaMarca.isEmpty()) {
			throw new NotFoundException();
		}
		Marca updateMarca = buscaMarca.get();
		updateMarca.nome = marca.nome;
		updateMarca.persist();
	}

	public void apagaMarca(Long marcaId) {
		Optional<Marca> buscaMarca = Marca.findByIdOptional(marcaId);
		buscaMarca.ifPresentOrElse(Marca::delete, () ->{throw new NotFoundException();});		
	}
}
