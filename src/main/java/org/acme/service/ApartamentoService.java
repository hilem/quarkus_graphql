//package org.acme.service;
//
//import java.util.List;
//
//import javax.enterprise.context.ApplicationScoped;
//
//import org.acme.model.Apartamento;
//
//@ApplicationScoped
//public class ApartamentoService {
//	
//	public List<Apartamento> buscaTodosApartamentos (){
//		return Apartamento.listAll();
//	}
//	
//	public Apartamento buscaApartamento(Long apartamentoId) {
//		return Apartamento.findById(apartamentoId);
//	}
//	public void insereApartamento(Apartamento apartamento) {
//		Apartamento.persist(apartamento);
//
//	}
//
//	public Apartamento atualizaApartamento(Apartamento apartamento) {
//		Apartamento updateApartamento = Apartamento.findById(apartamento.id);
//		if(!updateApartamento.equals(null)) {
//			Apartamento.update("numero = ?1, bloco = ?2, andar = ?3 WHERE id = ?4",apartamento.numero, apartamento.bloco,apartamento.andar, apartamento.id);		
//		}
//		return updateApartamento;
//	}
//
//	public Apartamento apagaApartamento(Long apartamentoId) {
//		Apartamento buscaApartamento = Apartamento.findById(apartamentoId);
//		if(!buscaApartamento.equals(null)){
//		  Apartamento.deleteById(apartamentoId);
//		}
//		return buscaApartamento;		
//	}
//
//}
