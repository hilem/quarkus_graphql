//package org.acme.service;
//
//import java.util.List;
//
//import javax.enterprise.context.ApplicationScoped;
//
//import org.acme.model.Condominio;
//
//@ApplicationScoped
//public class CondominioService {
//	
//	public List<Condominio> buscaTodosCondominios (){
//		return Condominio.listAll();
//	}	
//	public Condominio buscaCondominio(Long condominioId) {
//		return Condominio.findById(condominioId);
//	}
//	public List<Condominio> buscaCondominioNome(String condominio) {
//		return Condominio.find("nome LIKE ?1  ", "%"+condominio+"%").list();
//	}
//	public void insereCondominio(Condominio condominio) {
//		Condominio.persist(condominio);
//	}
//	public Condominio atualizaCondominio(Condominio condominio) {
//		Condominio updateCondominio = Condominio.findById(condominio.id);
//		if(!updateCondominio.equals(null)) {
//			Condominio.update("nome = ?1 WHERE id = ?2", condominio.nome,condominio.id);		
//		}
//		return updateCondominio;
//	}
//	public Condominio apagaCondominio(Long condominioId) {
//		Condominio buscaCondominio = Condominio.findById(condominioId);
//		if(!buscaCondominio.equals(null)){
//		  Condominio.deleteById(condominioId);
//		}
//		return buscaCondominio;		
//	}
//}
