package org.acme.controller;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.acme.model.Marca;
import org.acme.service.MarcaService;

import io.vertx.core.cli.annotations.Description;

@Path("/marcas")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)

public class MarcaController {
	
	@Inject
    MarcaService marcaService ;
    
	@GET
	@Description("Buscar uma lista de marcas") 
	public List<Marca> todasMarcas() {		
			return marcaService.buscaTodasMarcas();
	}
	@GET
	@Path("/id/{marcaId}")
	@Description("Buscar apenas uma marca") 
	public Marca marca(@PathParam("marcaId") Long marcaId) {		
			return marcaService.buscaMarca(marcaId);
	}
	@GET
	@Path("/nome/{nome}")
	@Description("Buscar marca pelo nome") 
	public List<Marca> marcaNome(@PathParam("nome") String nome) {		
			return marcaService.buscaMarcaNome(nome);
	}
	@POST
	@Transactional
	@Description("Faz o cadastro de uma marca") 
	public Response adicionar(Marca marca) {		
		marcaService.insereMarca(marca);
		return Response.status(Status.CREATED).build();
	}
	@PUT
	@Transactional
	@Path("{marcaId}")
	@Description("Atualiza o registro  de uma marca") 
	public void atualizarMarca(@PathParam("marcaId") Long marcaId, Marca marca) {
		 marcaService.atualizaMarca(marcaId, marca);
	}
	@DELETE
	@Transactional
	@Path("{marcaId}")
	@Description("Apaga registro") 
	public void deletarMarca(@PathParam("marcaId") Long marcaId) {
		 marcaService.apagaMarca(marcaId);
	}	

}
