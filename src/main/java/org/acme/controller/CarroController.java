//package org.acme.controller;
//
//import java.util.List;
//
//import javax.inject.Inject;
//import javax.transaction.Transactional;
//import javax.ws.rs.Consumes;
//import javax.ws.rs.Produces;
//import javax.ws.rs.core.MediaType;
//
//import org.acme.model.Carro;
//import org.acme.service.CarroService;
//import org.eclipse.microprofile.graphql.GraphQLApi;
//import org.eclipse.microprofile.graphql.Mutation;
//import org.eclipse.microprofile.graphql.Query;
//
//import io.vertx.core.cli.annotations.Description;
//
//
//@Produces(MediaType.APPLICATION_JSON)
//@Consumes(MediaType.APPLICATION_JSON)
//@GraphQLApi
//public class CarroController {
//	
//	@Inject
//    CarroService carroService ;
//    
//	@Query("buscaTodasCarros")
//	@Description("Buscar uma lista de Carros") 
//	public List<Carro> buscarTodasCarros() {		
//			return carroService.buscaTodasCarros();
//	}
//	@Query("buscaCarro")
//	@Description("Buscar apenas uma Carro") 
//	public Carro buscarCarro(Long CarroId) {		
//			return carroService.buscaCarro(CarroId);
//	}
//	@Query("buscaCarroNome")
//	@Description("Buscar Carro pelo nome") 
//	public List<Carro> buscarCarroNome(String Carro) {		
//			return carroService.buscaCarroNome(Carro);
//	}
//	@Mutation
//	@Transactional
//	@Description("Faz o cadastro de uma Carro") 
//	public Carro cadastrarCarro(Carro Carro) {		
//		carroService.insereCarro(Carro);
//		return Carro;
//	}
//	@Mutation
//	@Transactional
//	@Description("Atualiza o registro  de uma Carro") 
//	public Carro atualizarCarro(Carro Carro) {
//		return carroService.atualizaCarro(Carro);
//	}
//	@Mutation
//	@Transactional
//	@Description("Apaga registro") 
//	public Carro deletarCarro(Long CarroId) {
//		return carroService.apagaCarro(CarroId);
//	}	
//
//}
