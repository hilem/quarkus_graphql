//package org.acme.controller;
//
//import java.util.List;
//
//import javax.inject.Inject;
//import javax.transaction.Transactional;
//import javax.ws.rs.Consumes;
//import javax.ws.rs.Produces;
//import javax.ws.rs.core.MediaType;
//
//import org.acme.model.Condominio;
//import org.acme.service.CondominioService;
//import org.eclipse.microprofile.graphql.GraphQLApi;
//import org.eclipse.microprofile.graphql.Mutation;
//import org.eclipse.microprofile.graphql.Query;
//
//import io.vertx.core.cli.annotations.Description;
//
//@Produces(MediaType.APPLICATION_JSON)
//@Consumes(MediaType.APPLICATION_JSON)
//@GraphQLApi
//public class CondominioController {
//	@Inject
//	CondominioService condominioService;
//	
//	@Query("buscaTodosCondominio")
//	@Description("Busca uma lista de condominio")
//	public List<Condominio> buscaTodosCondominio(){
//		return condominioService.buscaTodosCondominios();
//	}
//	@Query("buscaCondominio")
//	@Description("Busca um condominio")
//	public Condominio buscaCondominio(Long condominioId) {
//		return condominioService.buscaCondominio(condominioId);
//	}
//	@Query("buscaCondominioPorNome")
//	@Description("Busca condonios pelo nome")
//	public List<Condominio> buscaCondominioPorNome(String nome){
//		return condominioService.buscaCondominioNome(nome);
//	}
//	@Mutation
//	@Transactional
//	@Description("Cria um condominio")
//	public Condominio cadastraCondominio(Condominio condominio) {
//		condominioService.insereCondominio(condominio);
//		return condominio;
//	}
//	@Mutation
//	@Transactional
//	@Description("Atualiza um condominio")
//	public Condominio atualizaCondominio(Condominio condominio) {
//		return condominioService.atualizaCondominio(condominio);
//	}
//	@Mutation
//	@Transactional
//	@Description("Apaga um condominio")
//	public Condominio deletaCondominio(Long condominioId) {
//		return condominioService.apagaCondominio(condominioId);
//	} 
//}
