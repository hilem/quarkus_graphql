package org.acme.controller;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.acme.model.Modelo;
import org.acme.service.ModeloService;

import io.vertx.core.cli.annotations.Description;

@Path("/modelos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ModeloController {
	@Inject
    ModeloService modeloService ;
    
	@GET
	@Description("Buscar uma lista de modelos de carro") 
	public List<Modelo> todosModelos() {		
			return modeloService.buscaTodosModelo();
	}
	@GET
	@Path("/id/{modeloId}")
	@Description("Buscar apenas um modelo de carro") 
	public Modelo modelo(@PathParam("modeloId") Long modeloId) {		
			return modeloService.buscaModelo(modeloId);
	}
	@GET
	@Path("/nome/{nome}")
	@Description("Buscar modelo pelo nome") 
	public List<Modelo> modeloNome(@PathParam("nome") String nome) {		
			return modeloService.buscaModeloNome(nome);
	}
	@POST
	@Transactional
	@Description("Faz o cadastro de um modelo de carro") 
	public Response cadastrarModelo(Modelo modelo) {		
		modeloService.insereModelo(modelo);
		return Response.status(Status.CREATED).build();
	}
	@PUT
	@Transactional
	@Path("{modeloId}")
	@Description("Atualiza o registro  de um modelo") 
	public void atualizarModelo(@PathParam("modeloId") Long modeloId, Modelo modelo) {
		 modeloService.atualizaModelo(modeloId, modelo);
	}
	@DELETE
	@Path("{modeloId}")
	@Transactional
	@Description("Apaga registro") 
	public void deletarModelo(@PathParam("modeloId") Long modeloId) {
		 modeloService.apagaModelo(modeloId);
	}

}
