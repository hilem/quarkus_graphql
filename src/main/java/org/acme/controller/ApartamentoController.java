//package org.acme.controller;
//
//import java.util.List;
//
//import javax.inject.Inject;
//import javax.transaction.Transactional;
//import javax.ws.rs.Consumes;
//import javax.ws.rs.Produces;
//import javax.ws.rs.core.MediaType;
//
//import org.acme.model.Apartamento;
//import org.acme.service.ApartamentoService;
//import org.eclipse.microprofile.graphql.GraphQLApi;
//import org.eclipse.microprofile.graphql.Mutation;
//import org.eclipse.microprofile.graphql.Query;
//
//import io.vertx.core.cli.annotations.Description;
//
//@Produces(MediaType.APPLICATION_JSON)
//@Consumes(MediaType.APPLICATION_JSON)
//@GraphQLApi
//public class ApartamentoController {
//	@Inject
//	ApartamentoService ApartamentoService;
//	
//	@Query("buscaTodosApartamento")
//	@Description("Busca uma lista de apartamento")
//	public List<Apartamento> buscaTodosApartamento(){
//		return ApartamentoService.buscaTodosApartamentos();
//	}
//	@Query("buscaApartamento")
//	@Description("Busca um apartamento")
//	public Apartamento buscaApartamento(Long apartamentoId) {
//		return ApartamentoService.buscaApartamento(apartamentoId);
//	}
//
//	@Mutation
//	@Transactional
//	@Description("Cria um apartamento")
//	public Apartamento cadastraApartamento(Apartamento apartamento) {
//		ApartamentoService.insereApartamento(apartamento);
//		return apartamento;
//	}
//	@Mutation
//	@Transactional
//	@Description("Atualiza um apartamento")
//	public Apartamento atualizaApartamento(Apartamento apartamento) {
//		return ApartamentoService.atualizaApartamento(apartamento);
//	}
//	@Mutation
//	@Transactional
//	@Description("Apaga um apartamento")
//	public Apartamento deletaApartamento(Long apartamentoId) {
//		return ApartamentoService.apagaApartamento(apartamentoId);
//	} 
//}
