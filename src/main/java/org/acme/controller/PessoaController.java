//package org.acme.controller;
//
//import java.util.List;
//
//import javax.inject.Inject;
//import javax.transaction.Transactional;
//import javax.ws.rs.Consumes;
//import javax.ws.rs.Produces;
//import javax.ws.rs.core.MediaType;
//
//import org.acme.model.Pessoa;
//import org.acme.service.PessoaService;
//import org.eclipse.microprofile.graphql.GraphQLApi;
//import org.eclipse.microprofile.graphql.Mutation;
//import org.eclipse.microprofile.graphql.Query;
//
//import io.vertx.core.cli.annotations.Description;
//
//
//@Produces(MediaType.APPLICATION_JSON)
//@Consumes(MediaType.APPLICATION_JSON)
//@GraphQLApi
//public class PessoaController {
//    @Inject
//    PessoaService pessoaService;
//    
//	@Query("buscaTodasPessoas")
//	@Description("Buscar uma lista de pessoa") 
//	public List<Pessoa> buscarTodasPessoas() {		
//			return pessoaService.buscaTodasPessoa();
//	}
//	@Query("buscaPessoa")
//	@Description("Buscar apenas uma pessoa") 
//	public Pessoa buscarPessoa(int idPessoa) {		
//			return pessoaService.buscaPessoa(idPessoa);
//	}
//	@Mutation
//	@Transactional
//	@Description("Faz o cadastro de uma pessoa") 
//	public Pessoa cadastrarPessoa(Pessoa pessoa) {		
//		pessoaService.inserePessoa(pessoa);
//		return pessoa;
//	}
//	@Mutation
//	@Transactional
//	@Description("Atualiza o registro  de um pessoa") 
//	public Pessoa atualizarPessoa(Pessoa pessoa) {
//		return pessoaService.atualizaPessoa(pessoa);
//	}
//	@Mutation
//	@Transactional
//	@Description("Apaga registro") 
//	public Pessoa deletarPessoa(int pessoaId) {
//		return pessoaService.apagaPessoa(pessoaId);
//	}
//
//}
